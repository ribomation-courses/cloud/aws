# AWS Overview, 2 days
Welcome to this course.
The syllabus can be found at
[cloud/aws-overview](https://www.ribomation.se/courses/cloud/aws-overview.html)

Here you can find
* Installation instructions
* Start-code for the practical exercises

# AWS Account
In order to interact with AWS and perform the exercises, you need
to sign-up for an AWS account. Please, do this before the course
starts. You probably need to provide a credit/debit card to cover
any excess spending. 
* [Sign-Up for AWS](https://portal.aws.amazon.com/billing/signup#/start)

The exercises in the course are not intended to cost anything. In
most cases, you will be using the AWS Free Tier.

Just remember to shut down any running services, that are charged
per hour, such as EC2 servers and RDS databases.

# Installation Instructions
It's highly recommended that you do the exercises in a Unix/Linux
like environment, such as `WSL @ Windows 10`, `BASH for Windows GIT`,
Linux running in a _Virtual Machine_ or _Docker_ or similar.

The primary tools you need are
* SSH Client (to login to an EC2 server)
* SCP Client (to copy files to an EC2 server)
* AWS CLI (interacting with AWS from the command-line)
* A text editor or small IDE, such as MS Visual Code
* A modern browser, such as Chrome, Edge or FireFox

For some exercises it advisable to have the following installed
* Node.js version 10.x
* ZIP client, using the PKZIP format
* [HeidiSQL DB Client](https://www.heidisql.com/)

If you have a *NIX like environment you probably already have
access to `ssh`, `scp` and `zip`, else you need to find another solution,
such as PuTTY or similar.

We will install AWS CLI during the course. It's an installer,
that unpacks a bundled Python environment together with the
CLI tool. Ensure you have permissions to install software on
your laptop.

Install  [Microsoft Visual Code](https://code.visualstudio.com/) 
or another IDE or text editor, before the course starts.

Also install [Node.js, version 10.x](https://nodejs.org/en/download/),
before the course starts.

Finally, install the DB client above or use one you are more
familiar with. It must be able to connect to a remote MySQL
database.

# Proxy and/or VPN
If your laptop is using any corporate network software, that limits
your connectivity. You need to ensure you can perform SSH access (port 22)
to AWS and connect to a DB (port 3306).


# Course GIT Repo
Install a [GIT Client](https://git-scm.com/downloads).
Then create a base folder for this course and create a sub-folder
for your solutions and clone the repo as another sub-folder.

    mkdir -p /path/to/some/folder/aws-course
    cd $_
    mkdir my-solutions
    git clone https://gitlab.com/ribomation-courses/cloud/aws.git gitlab

When there are updates, perform a `git pull` operation

    cd /path/to/some/folder/aws-course/gitlab
    git pull


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

