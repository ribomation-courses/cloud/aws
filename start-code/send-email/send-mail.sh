#!/usr/bin/env bash
set -eux

say() { echo "$0: $*" >&2; }
die() { say "$*"; exit 1; }

which aws || die "cannot find the aws cli"

FROM=<validated email>
TO=<validated email>
SUBJECT="Test AWS CLI & SES"
MESSAGE="Hi there from AWS SES"

aws ses send-email \
    --from "$FROM" \
    --to "$TO" \
    --subject "$SUBJECT" \
    --text "$MESSAGE"
