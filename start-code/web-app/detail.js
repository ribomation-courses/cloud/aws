$(() => {
    'use strict';
    const baseUrl = 'https://randomuser.me/api/';

    function populate(person) {
        $('#avatar').attr('src', person.picture.large);
        $('#name').append(`${person.name.title} ${person.name.first} ${person.name.last} `);
        $('#email').append(`${person.email}`);
        $('#phone').append(`${person.phone}`);
        $('#address').append(`
            ${person.location.street.number} ${person.location.street.name}<br/
             ${person.location.postcode} ${person.location.city}<br/>
             ${person.location.state} ${person.location.country} (${person.nat})
        `);
        $('#account').append(`
            <code>${person.login.username}</code><br/>
            (<em>since ${new Date(person.registered.date).toLocaleDateString()}</em>)
        `);
    }

    function loadPerson(seed, row, size) {
        //const numberOfPersons = +row + 1;
        $.ajax({
            url: `${baseUrl}?seed=${seed}&results=${size}`,
            dataType: 'json',
            success: (payload) => {
                const person = payload.results[row];
                populate(person);
            }
        });
    }

    function params() {
        return location.search
            .substr(1)
            .split('&')
            .reduce((params, param) => {
                const pair      = param.split('=');
                params[pair[0]] = pair[1];
                return params;
            }, {});
    }

    function load() {
        const q = params();
        console.debug('params: ', q);
        loadPerson(q.seed, q.row, q.size);
    }

    load();
});
